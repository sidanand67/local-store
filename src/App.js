import { React, Component } from "react";
import Products from "./components/Products/Products";
import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
import Description from "./components/Products/Description";
import Header from "./components/Header/Header";
import { NoMatch } from "./components/NoMatch/NoMatch";
import CreateProduct from "./components/CreateProduct/CreateProduct";
import axios from "axios";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: "LOADING",
            products: [],
        };
    }

    componentDidMount() {
        axios
            .get("https://fakestoreapi.com/products")
            .then((response) => {
                this.setState({
                    products: response.data,
                    status: "LOADED",
                });
            })
            .catch((err) => {
                this.setState({
                    status: "ERROR",
                });
            });
    }

    createProduct = (newProduct) => {
        let stateProduct = [...this.state.products]; 

        newProduct.id = this.state.products.length+1; 

        stateProduct.push(newProduct);

        this.setState({
            products: stateProduct
        });

    }

    render() {
        console.log(this.state.products); 
        return (
            <Routes>
                <Route path="/" element={<Header />}>
                    <Route index element={<Products status={this.state.status} products={this.state.products} />} />
                    <Route
                        path="product/:productId"
                        element={<Description />}
                    />
                    <Route path="create" element={<CreateProduct createProduct={this.createProduct}/>} />
                    <Route path="*" element={<NoMatch />} />
                </Route>
            </Routes>
        );
    }
}

export default App;

