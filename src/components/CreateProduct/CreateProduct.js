import "./CreateProduct.css";
import React, {Component} from "react";
import validator from "validator";

class CreateProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            titleError: undefined, 

            price: "",
            priceError: undefined, 

            description: "",
            descriptionError: undefined, 

            category: "",
            categoryError: undefined, 

            rate: "",
            rateError: undefined, 

            count: "",
            countError: undefined, 

            image: "",
            imageError: undefined
        };
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        }); 
    }

    validateFields = (event) => {
        event.preventDefault();

        const stateObj = {}
        let hasErrors = false; 

        if (event.target.title.value.trim().length === 0) {
            stateObj.titleError = "Title field can not be empty";
            hasErrors = true; 
        } else {
            stateObj.titleError = undefined; 
        }

        if (event.target.price.value.trim().length === 0) {
            stateObj.priceError = "Price field can not be empty";
            hasErrors = true; 
        } else if (!validator.isFloat(event.target.price.value.trim())) {
            stateObj.priceError = "Price field only accepts decimal value";
            hasErrors = true; 
        } else {
            stateObj.priceError = undefined;
        }

        if (event.target.description.value.trim().length === 0) {
            stateObj.descriptionError = "Description field can not be empty";
            hasErrors = true; 
        } else {
            stateObj.descriptionError = undefined;
        }

        if (event.target.category.value.trim().length === 0) {
            stateObj.categoryError = "Category field can not be empty";
            hasErrors = true; 
        } else {
            stateObj.categoryError = undefined;
        }

        if (event.target.rate.value.trim().length === 0) {
            stateObj.rateError = "Rate field can not be empty";
            hasErrors = true; 
        } else if (
            !validator.isFloat(event.target.rate.value.trim(), {
                min: 0.0,
                max: 5.0,
            })
        ) {
            stateObj.rateError =
                "Rate field only accepts values between 0 to 5";
            hasErrors = true; 
        } else {
            stateObj.rateError = undefined;
        }

        if (event.target.count.value.trim().length === 0) {
            stateObj.countError = "Count field can not be empty";
            hasErrors = true; 
        } else if (!validator.isFloat(event.target.count.value.trim())) {
            stateObj.countError = "Count field only accepts decimal value";
            hasErrors = true; 
        } else {
            stateObj.countError = undefined;
        }

        if (event.target.image.value.trim().length === 0) {
            stateObj.imageError = "Image URL field can not be empty";
            hasErrors = true; 
        } else if (
            !validator.isURL(event.target.image.value.trim(), {
                allow_underscores: true,
            })
        ) {
            stateObj.imageError = "Image URL field only accepts web links";
            hasErrors = true; 
        } else {
            stateObj.imageError = undefined;
        }
        
        this.setState(stateObj); 

        if (!hasErrors){

            const newProduct = {
                title: this.state.title,
                price: Number(this.state.price),
                description: this.state.description,
                category: this.state.category,
                rating: {
                    rate: Number(this.state.rate),
                    count: this.state.count,
                },
                image: this.state.image,
            };
    
            this.props.createProduct(newProduct);
        }

    };

    render() {
        return (
            <form className="form-container" onSubmit={this.validateFields}>

                <h1 className="heading">Add New Product</h1>

                <input
                    type="text"
                    name="title"
                    placeholder="Enter Product Title"
                    onChange={this.handleChange}
                />
                {this.state.titleError ? (
                    <div className="error">{this.state.titleError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}

                <input
                    type="text"
                    name="price"
                    placeholder="Enter Product Price"
                    onChange={this.handleChange}
                />
                {this.state.priceError ? (
                    <div className="error">{this.state.priceError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}

                <input
                    type="text"
                    name="description"
                    placeholder="Enter Product Description"
                    onChange={this.handleChange}
                />
                {this.state.descriptionError ? (
                    <div className="error">{this.state.descriptionError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}

                <input
                    type="text"
                    name="category"
                    placeholder="Enter Product Category"
                    onChange={this.handleChange}
                />
                {this.state.categoryError ? (
                    <div className="error">{this.state.categoryError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}

                <input
                    type="text"
                    name="rate"
                    placeholder="Enter Product Rating"
                    onChange={this.handleChange}
                />
                {this.state.rateError ? (
                    <div className="error">{this.state.rateError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}

                <input
                    type="text"
                    name="count"
                    placeholder="Enter Product Total Rating"
                    onChange={this.handleChange}
                />
                {this.state.countError ? (
                    <div className="error">{this.state.countError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}

                <input
                    type="url"
                    name="image"
                    placeholder="Enter Product Image URL"
                    onChange={this.handleChange}
                />
                {this.state.imageError ? (
                    <div className="error">{this.state.imageError}</div>
                ) : (
                    <div className="error">&nbsp;</div>
                )}
                <button>Create</button>
            </form>
        );
    }
};

export default CreateProduct;
