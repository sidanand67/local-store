import React from 'react'; 
import { Outlet, Link } from 'react-router-dom';
import './Header.css'; 

const Header = (props) => {
    return (
        <>
            <header>
                <Link to="/">
                    <h1>
                        Local <i className="fa-solid fa-shop store-icon"></i> Store
                    </h1>
                </Link>
                <Link to="/create">
                    <h4>Add New Product</h4>
                </Link>
            </header>
            <Outlet />
        </>

    );
}

export default Header; 