import './NoMatch.css'; 

export const NoMatch = () => {
    return (
        <div className="error">
            <i class="fa-solid fa-xmark xmark"></i> Page Not Found.
        </div>
    );
}