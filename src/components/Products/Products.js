import React from "react";
import Loader from "../Loader/Loader";
import "./Products.css";
import { Link } from "react-router-dom";

const Products = (props) => {

    const errorMsg = (
        <div className="error">
            <i class="fa-solid fa-xmark xmark"></i> Sorry, can't connect at
            the moment.
        </div>
    );

    const productsRender =
        props.products.length > 0 ? (
            props.products.map((product) => {
                return (
                    <Link
                        to={`/product/${product.id}`}
                        state={{ from: props.products }}
                    >
                        <div className="card" key={product.id}>
                            <img src={product.image} alt="product" />
                            <div className="product-text">
                                <p className="category">
                                    {product.category}
                                </p>
                                <p className="title">{product.title.slice(0,50)}</p>
                                <p className="rating">
                                    <i className="fa-solid fa-star star"></i>
                                    {product.rating.rate} (
                                    {product.rating.count} users rated)
                                </p>
                                <p className="price">
                                    <i className="fa-solid fa-dollar-sign dollar"></i>
                                    {product.price.toFixed(2)}
                                </p>
                            </div>
                        </div>
                    </Link>
                );
            })
        ) : (
            <div className="error">
                <i className="fa-solid fa-xmark xmark"></i> No products
                found.
            </div>
        );

    return (
        <>
            <div className="container">
                {props.status === "LOADING" && <Loader />}
                {props.status === "ERROR" && errorMsg}
                {props.status === "LOADED" && productsRender}
            </div>
        </>
    );
}

export default Products;
