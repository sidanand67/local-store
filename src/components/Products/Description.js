import { useParams, useLocation } from "react-router-dom";
import './Description.css'; 

const Description = () => {
    const { productId } = useParams();
    const location = useLocation();
    const { from } = location.state;

    return (
        <>
            <div className="box" key={from.productId}>

                <img src={from[productId - 1].image} alt="product" />

                <div className="card-text">

                    <p className="card-title">{from[productId - 1].title}</p>
                   
                    <p className="card-category">{from[productId - 1].category}</p>
                   
                    <p className="card-rating">
                        <i className="fa-solid fa-star star"></i>
                        {from[productId - 1].rating.rate} (
                        {from[productId - 1].rating.count} users rated)
                    </p>
                    <p className="card-price">
                        <i className="fa-solid fa-dollar-sign dollar"></i>
                        {from[productId - 1].price.toFixed(2)}
                    </p>
                    <p className="desc-tag">Description:</p>
                    <p className="card-description">
                        {from[productId - 1].description}
                    </p>
                </div>
            </div>
        </>
    );
};

export default Description;
